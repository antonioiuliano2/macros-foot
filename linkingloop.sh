#!/bin/bash

if [[ $# -eq 0 ]] ; then
    echo 'Script for performing the two required linking steps. Usage is: '
    echo ' '
    echo 'source linkingloop.sh nfrom_plate nto_plate'
    echo ' '
    echo 'just replace nfrom_plate and nto_plate with corresponding numbers'
    return 0
fi

cp firstlink.rootrc link.rootrc
makescanset -set=2.0.0.0 -dzbase=178 -from_plate=$1 -to_plate=$2 -v=2

echo "Starting pre-linking"

emlink -set=2.0.0.0 -new -v=2

cp b000002.0.0.0.link.pdf b000002.0.0.0.prelink.pdf

cp secondlink.rootrc link.rootrc
makescanset -set=2.0.0.0 -dzbase=178 -from_plate=$1 -to_plate=$2 -v=2

echo "Starting true linking"

emlink -set=2.0.0.0 -new -v=2
