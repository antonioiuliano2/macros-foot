/*Usage notes:
 scp /Users/Giuliana/Desktop/Uni/FOOT/GSI_data/trk_puritytest.C scanner@nusrv9.na.infn.it:/home/scanner/foot/2019_GSI/GSI1/b000011
 
 scp /Users/Giuliana/Desktop/Uni/FOOT/GSI_data/trk_puritytest.C scanner@nusrv9.na.infn.it:/home/scanner/foot/2019_GSI/macros

 pw: neutrino.01
 
 root -l
 .L trk_puritytest.C+
 trk_puritytest()
 
 */

#define GSI 1
#define FROM_PLATE 31
#define TO_PLATE 66
#define EVERBOSE 0
#define DATA 1
#define MC 1
#define PLOT 1

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <map>
#include <string>
#include <utility>
#include <string>
#include "TH1.h"
#include "TCanvas.h"
#include "TVector.h"
#include "TVector3.h"
#include "TLegend.h"
#include "TFile.h"
#include "TString.h"
#include "TObjString.h"
#include "TSystem.h"
#include "TROOT.h"
//FEDRA libraries
#include "/mnt/sdb/opera/ana/valeri/sw_oracle_dev/fedra/include/EdbSegP.h"
#include "/mnt/sdb/opera/ana/valeri/sw_oracle_dev/fedra/include/EdbPattern.h"



void trk_puritytest() {
    //  gStyle->SetOptStat("nemr");
    
    
    float cut_OXY=0.04;
    float cut_theta=0.1;
    float cut_nseg=3;
    
    const int BRICKID=GSI+GSI*10;
    
    const int TOT_PLATE=TO_PLATE-FROM_PLATE+1;
    
    char path[100];
    sprintf(path, "/home/scanner/foot/2019_GSI/GSI%d/b%06d/b%06d.1.0.0.trk.root", GSI, BRICKID, BRICKID);
    
    TFile *file = new TFile(path);
    TTree *tree = (TTree*)file->Get("tracks");
    
    cout << "Using file DATA: " << path << endl;
    
    int brick_MC = BRICKID-BRICKID*10;
    sprintf(path, "/home/scanner/foot/RECO_MC_TEST_giul/b%06d/b%06d.1.0.0.trk.root", GSI,GSI);
    TFile *file_MC = new TFile(path);
    
    TTree *tree_MC = (TTree*)file_MC->Get("tracks");
    
    cout << "Using file MC: " << path << endl;
    
    EdbSegP *tr, *seg;
    TClonesArray *s  = new TClonesArray("EdbSegP");
    TClonesArray *sf  = new TClonesArray("EdbSegP");
    
    int nseg=0, npl=0, n0=0;
    int minplate=0, maxplate=0;
    float w=0;
    tree->SetBranchAddress("t.",&tr);
    tree->SetBranchAddress("s",&s); //array of measured segments
    tree->SetBranchAddress("sf",&sf); //array of fitted segments
    tree->SetBranchAddress("nseg",&nseg); //total number of segments
    tree->SetBranchAddress("npl",&npl); //total number of plates passed by track
    tree->SetBranchAddress("n0",&n0); //number of "holes" - plates where the segments were not found. In principle n0 should always be npl-nseg
    tree->SetBranchAddress("w",&w); //the sum of all s.eW
    
    tree_MC->SetBranchAddress("t.",&tr);
    tree_MC->SetBranchAddress("s",&s); //array of measured segments
    tree_MC->SetBranchAddress("sf",&sf); //array of fitted segments
    tree_MC->SetBranchAddress("nseg",&nseg); //total number of segments
    tree_MC->SetBranchAddress("npl",&npl); //total number of plates passed by track
    tree_MC->SetBranchAddress("n0",&n0); //number of "holes" - plates where the segments were not found. In principle n0 should always be npl-nseg
    tree_MC->SetBranchAddress("w",&w); //the sum of all s.eW
    
    
    Long64_t nentries = tree->GetEntries();
    cout << "Number of entries DATA: " << nentries << endl;
    
    Long64_t nentries_MC = tree_MC->GetEntries();
    cout << "Number of entries MC: " << nentries_MC << endl;
    
    int trk_evt,trk_trk;
    const int NPLATE=TO_PLATE-FROM_PLATE;
    const int VECPLACE_TOT=NPLATE/5;
    float meanperc[VECPLACE_TOT]={0};
    int nperc[VECPLACE_TOT]={0};
    
    for (Long64_t itr=0;itr<nentries; itr++) { //nentries
        tree_MC->GetEntry(itr);
        //Nwrong=0;
        map<pair<int,int>, int> frequencyEvent;
        
        // frequencyEvent.clear();
        
        if(nseg>=cut_nseg){
            for(Long64_t iseg=0;iseg<nseg; iseg++){
                seg=(EdbSegP*)s->At(iseg);
                trk_evt=seg->MCEvt();
                trk_trk=seg->MCTrack();
                
                if (frequencyEvent.find(pair<int,int>(trk_evt, trk_trk)) == frequencyEvent.end()) //the first time does not exist yet, I create it with value 0
                    frequencyEvent[pair<int,int>(trk_evt,trk_trk)] = 0;
                frequencyEvent[pair<int,int>(trk_evt, trk_trk)]++;
            }
            
            map<pair<int,int>, int>::iterator it;
            int nseg_ok = 0;
            
            for (it = frequencyEvent.begin(); it != frequencyEvent.end(); it++){
                if(it->second > nseg_ok){
                    //                pair<int,int> ids = it->first;
                    //                int mostfrequentevent = ids.first;
                    //                int mostfrequenttrk = ids.second;
                    nseg_ok = it->second;
                }
            }
            
            int nsegwrong = nseg - nseg_ok;
            float percwrong = (float)nsegwrong/nseg;
            float percok = (float)nseg_ok/nseg;
            int vecplace = nseg/5;
            meanperc[vecplace]+=percok;
            nperc[vecplace]++;
            //cout << nseg << "\t" << nseg_ok << "\t" << vecplace << "\t" << meanperc[vecplace] << "\t" << nperc[vecplace] << endl;
            
            //                for (it = frequencyEvent.begin(); it != frequencyEvent.end(); it++){
            //                    pair<int,int> ids = it->first;
            //                    if(ids.first!=mostfrequentevent || ids.second!=mostfrequenttrk) Nwrong+=it->second;
            //                }
        }
    }
    
    
    for (int i=0; i<VECPLACE_TOT; i++){
        cout << i*5 << "\t" << (float)meanperc[i]/nperc[i] << endl;
    }
    
}

