#!/bin/bash

if [[ $# -eq 0 ]] ; then
    echo 'Script for performing the two required linking steps for each plate (plate by plate). Usage is: '
    echo ' '
    echo 'source linkingloop_platebyplate.sh nfrom_plate nto_plate (nfrom_plate<=nto_plate)'
    echo ' '
    echo 'just replace nfrom_plate and nto_plate with corresponding numbers'
    return 0
fi

for i in $(seq -w $1 $2);

do
  source linkingloop.sh $i $i
done
