#!/bin/bash
startdir=$PWD
remotedir=/home/scanner/foot/2019_GSI/GSI2

b=22
brick=$(seq -f "%06g" $b $b)
major=0
minor=0
remotedir=$remotedir/b$brick

id=$brick.0.$major.$minor

mkdir b$brick
cd    b$brick
cp -r $remotedir/AFF AFF

for a in {1..30}
 do
 aa=$(seq -f "%03g" $a $a)
 mkdir p$aa
 cd p$aa
 ln -s $remotedir/p$aa/$b.$a.$major.$minor.raw.root $b.$a.$major.$minor.raw.root
 ln -s $remotedir/p$aa/$b.$a.$major.$minor.cp.root $b.$a.$major.$minor.cp.root
  cd ..
done
makescanset -set=$id -from_plate=30 -to_plate=1 -dzbase=180

cd $startdir