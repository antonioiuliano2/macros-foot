#!/bin/bash

# Basic if statement

for i in $(seq -w $1 $2)

  do

  echo $i

  if [ $i -le 100 ]
      
      then

      mkdir p0$i
      ln -s /mnt/data/FOOT/2019_GSI/mic5/GSI2/P00$i/tracks.raw.root /home/scanner/foot/2019_GSI/GSI2/b000002/p00$i/2.$i.0.0.raw.root
      echo link creato per la cartella p0$i
      
  elif [ $i -le 1000 ]

      then

      mkdir p0$i
      ln -s /mnt/data/FOOT/2019_GSI/mic5/GSI2/P0$i/tracks.raw.root /home/scanner/foot/2019_GSI/GSI2/b000002/p0$i/2.$i.0.0.raw.root
      echo link creato per la cartella p0$i

  else
      
      mkdir p$i
      ln -s /mnt/data/FOOT/2019_GSI/mic5/GSI2/P$i/tracks.raw.root /home/scanner/foot/2019_GSI/GSI2/b000002/p$i/2.$i.0.0.raw.root
      echo link creato per la cartella p0$i
      
  fi
  
done
