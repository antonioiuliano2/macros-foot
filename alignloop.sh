#!/bin/bash

if [[ $# -eq 0 ]] ; then
    echo 'Script for performing the two required alignment steps. Usage is: '
    echo ' '
    echo 'source alignloop.sh nfrom_plate nto_plate'
    echo ' '
    echo 'just replace nfrom_plate and nto_plate with corresponding numbers'
    return 0
fi

cp firstalign.rootrc align.rootrc
makescanset -set=2.0.0.0 -dzbase=178 -dz=-2400 -from_plate=$1 -to_plate=$2 -v=2

echo "Starting pre-align"

emalign -set=2.0.0.0 -new -v=2

cp b000002.0.0.0.align.pdf b000002.0.0.0.prealign.pdf

cp secondalign.rootrc align.rootrc
makescanset -set=2.0.0.0 -dzbase=178 -dz=-2400 -from_plate=$1 -to_plate=$2 -v=2

echo "Starting true align"

emalign -set=2.0.0.0 -new -v=2
