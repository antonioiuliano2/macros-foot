//Questo script confronta le tracce ricostruite con quelle del monte carlo true

/*USAGE:
scp /Users/Giuliana/Desktop/Uni/FOOT/TEST_RECO_MIC/fedra_MC_tracks_comparison.C scanner@nusrv9.na.infn.it:/home/scanner/foot/2019_GSI/macros
pw: neutrino.01

/*run the script:
 root -l compare_trks.C
 */

#define eVERBOSE 0
#define PLOT 1
#define NPLATES 30
#define brickid 2
#define DO_SCANBACK 0
#define eDRmax 50
#define eDTmax 0.07

#include <cstdlib>
#include <iostream>
#include <map>
#include <string>
#include "TH1.h"
#include "TCanvas.h"
#include <stdio.h>
#include <TROOT.h>
#include "TRandom3.h"
#include "TVector.h"
#include <vector>
#include "TTree.h"
#include "TFile.h"
#include "TClonesArray.h"
#include "/mnt/sdb/opera/ana/valeri/sw_oracle_dev/fedra/include/EdbCouplesTree.h"

#include "GSI_AnaMC.h"

int DR_OK(EMU_BaseTrack btMC, EMU_BaseTrack btMCnext);
int DT_OK(EMU_BaseTrack btMC, EMU_BaseTrack btMCnext);

float DR_x(EMU_BaseTrack btMC, EMU_BaseTrack btMCnext);
float DR_y(EMU_BaseTrack btMC, EMU_BaseTrack btMCnext);
float DR_2(EMU_BaseTrack btMC, EMU_BaseTrack btMCnext);
float DR_2(EdbSegP *btMC, EdbSegP *btMCnext);

float DT_x(EMU_BaseTrack btMC, EMU_BaseTrack btMCnext);
float DT_y(EMU_BaseTrack btMC, EMU_BaseTrack btMCnext);
float DT_2(EMU_BaseTrack btMC, EMU_BaseTrack btMCnext);
float DT_2(EdbSegP *btMC, EdbSegP *btMCnext);


//NB:
//seg->P() = seg momentum
//Aid(0) = pdgcode
//Aid(1) = motherID
//Vid(0) = firstplate
//Vid(1) = lastplate


void fedra_MC_tracks_comparison() {
    
    TFile *file_MC = new TFile("/home/scanner/foot/RECO_MC_TEST_giul/b000002/b000002.0.0.0.trk_S1_emtranew2.root");

    
    EMU_Event *evt = NULL;
    EMU_VolumeTrack vtMC;
    EMU_BaseTrack btMC, btMCnext, btMCpre;
    
    TFile *file_MC_ori = new TFile("Prod/EMU_Oxy200MeV_302710772010_C2H4S1_LxWWPbS3_beam3x3.root");
    
    TTree *EventTree = (TTree*)file_MC_ori->Get("EventTree");
    EventTree->SetBranchAddress("event", &evt);
    
    int COUNT_DR_OK=0, COUNT_DT_OK=0, COUNT_DRoDT_OK=0, COUNT_tot_DRDT = 0;
    
    if(DO_SCANBACK==1){
        //CP.ROOT
        
        char path_cp[100];
        EdbSegP *s_cp_MC[NPLATES+1];
        EdbSegP *s_cp;
        TFile *file_cp;
        TTree *tree_cp_MC[NPLATES+1];
        int nentries_cp_MC[NPLATES+1]={0};
        
        for(int icp=1; icp<=NPLATES; icp++){
            if (icp <10) sprintf(path_cp,"/home/scanner/foot/RECO_MC_TEST_giul/b000002/p00%i/12.%i.0.0.cp.root",icp, icp);
            else if(icp < 100) sprintf(path_cp, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/p0%i/12.%i.0.0.cp.root",icp,icp);
            else sprintf(path_cp,"/home/scanner/foot/RECO_MC_TEST_giul/b000002/p%i/12.%i.0.0.cp.root",icp, icp);
            
            file_cp = new TFile(path_cp, "READ");
            tree_cp_MC[icp] = (TTree*)file_cp->Get("couples");
            
            //tree_cp_MC[icp]->SetBranchAddress("s.",&s_cp_MC[icp]);
            
            tree_cp_MC[icp]->SetBranchAddress("s.",&s_cp);
            
            nentries_cp_MC[icp] = tree_cp_MC[icp]->GetEntries();
            cout << "CP MC pl " << icp << ": " << path_cp << "\tEntries: " << nentries_cp_MC[icp] << endl;
        }
    }
    
    
    TTree *tree_MCRECO = (TTree*)file_MC->Get("tracks");
    
    EdbSegP *tr=NULL, *seg=NULL;
    TClonesArray *s  = new TClonesArray("EdbSegP");
    TClonesArray *sf  = new TClonesArray("EdbSegP");
    int nseg=0, npl=0, n0=0, nseg_MC=0;
    int minplate=0, maxplate=0;
    float w=0;
    EdbSegP *tr1=NULL, *seg1=NULL, *seg_prev=NULL;
    TClonesArray *s1  = new TClonesArray("EdbSegP");
    TClonesArray *sf1  = new TClonesArray("EdbSegP");
    int nseg1=0, npl1=0, n01=0, nseg_MC1=0;
    int minplate1=0, maxplate1=0;
    float w1=0;
    float P_init=0, th_init=0;
    int ib_last=0, ib_init=0;
    
    int DR_OK_res=0, DT_OK_res=0;
    
    tree_MCRECO->SetBranchAddress("t.",&tr1);
    tree_MCRECO->SetBranchAddress("s",&s); //array of measured segments
    tree_MCRECO->SetBranchAddress("sf",&sf1); //array of fitted segments
    tree_MCRECO->SetBranchAddress("nseg",&nseg1); //total number of segments
    tree_MCRECO->SetBranchAddress("npl",&npl1); //total number of plates passed by track
    tree_MCRECO->SetBranchAddress("n0",&n01); //number of "holes" - plates where the segments were not found. In principle n0 should always be npl-nseg
    tree_MCRECO->SetBranchAddress("w",&w1); //the sum of all s.eW
    
    
    Long64_t nentries_MCRECO = tree_MCRECO->GetEntries();
    
    int N_ok=0, N_ok_plusdau=0;
    float frac=0, mean_frac=0;
    float frac_dau=0, mean_frac_dau=0;
    float fracMC=0, mean_fracMC=0;
    float frac_oxy=0, fracMC_oxy=0;
    int lastOK=0, firstOK=0, recoOK=0, tmp_firstOK=0, tmp_lastOK=0;
    int N_1=0, N_1_MC=0;
    int mcevent=0, mctrack=0, motherID=0, pdgcode=0;
    int lastoksegplate=0, firstoksegplate=0, refplate=0, firstplate=0, lastplate=0;
    int ib1=0, ib2=0, ib3=0;
    
    TH1F *h_frac = new TH1F("h_frac","frac; frac; entries" , 54, 0, 1.2);
    TH1F *h_fracMC = new TH1F("h_fracMC","frac_{MC}; frac_{MC}; entries" , 54, 0, 1.2);
    TH1F *h_nseg_nsegMC = new TH1F("h_nseg_nsegMC","N_{seg REC}/N_{seg MC}; N_{seg REC}/N_{seg MC}; entries" , 100, 0, 5);
    
    TH1F *h_frac_oxy = new TH1F("h_frac_oxy","frac_oxy; frac_oxy; entries" , 54, 0, 1.2);
    TH1F *h_fracMC_oxy = new TH1F("h_fracMC_oxy","frac_{MC}_oxy; frac_{MC}_oxy; entries" , 54, 0, 1.2);
    
    TH1F *h_P_NO = new TH1F("h_P_NO","P_{MC} tracce con frac_MC<0.6; P_{MC}; entries" , 30, 0, 15);
    TH2F *h_P_DP_NO = new TH2F("h_P_DP_NO","P_{MC} vs #DeltaP tra i seg mal collegato; P_{MC}; #DeltaP" , 100, 0, 10, 20, 0, 2);
    
    TH1F *h_DT2_all_OK = new TH1F("h_DT2_all_OK","#Delta#theta_{MC} all; #Delta#theta_{MC}^2; entries", 100, 0, 0.01);
    TH1F *h_DR2_all_OK = new TH1F("h_DR2_all_OK","#DeltaR^2_{MC} all; #DeltaR_{MC}^2;  entries", 200, 0, 1000);
    TH2F *h_DTmaxOK_P = new TH2F("h_DTmaxOK_P","#theta_{MC}max Vs P; #theta_{MC} max; P(GeV)" , 100, 0, 0.01, 60, 0, 15);
    TH2F *h_DRmaxOK_P = new TH2F("h_DRmaxOK_P","#DeltaR^2_{MC}max Vs P; #DeltaR^2_{MC} max;  P(GeV)", 200, 0, 1000, 60, 0, 15);
    
    
    TH1F *h_th_NO = new TH1F("h_th_NO","#theta_{MC} tracce con frac_MC<0.6; #theta_{MC}; entries" , 80, 0, 0.4);
    TH2F *h_th_Dth_NO = new TH2F("h_th_Dth_NO","#theta_{MC} vs #Delta#theta tra i seg mal collegato; #theta_{MC}; #Delta#theta" , 150, 0, 0.3, 50, 0, 0.1);
    
    TH2F *h_P_fracdau = new TH2F("h_P_fracdau","P_{MC} vs frac (dau included); P_{MC}; frac(dau incl)", 100, 0, 10., 54, 0, 1.2);
    TH2F *h_th_fracdau = new TH2F("h_th_fracdau","#theta_{MC} vs frac (dau included); #theta_{MC}; frac(dau incl)", 150, 0, 0.3, 54, 0, 1.2);
    
    TH1F *h_DT_spezzate = new TH1F("h_DT_spezzate","#DeltaT^2_{MC} tra i segmenti spezzati; #DeltaT^2_{MC}; entries", 100, 0, 0.004);
    TH1F *h_DR_spezzate = new TH1F("h_DR_spezzate","#DeltaR^2_{MC} tra i segmenti spezzati; #DeltaR^2_{MC}; entries", 250, 0, 10000);
    TH2F *h_DthDR_spezzate = new TH2F("h_DthDR_spezzate","#DeltaT^2_{MC} Vs #DeltaR^2_{MC} tra i segmenti spezzati; #DeltaT^2_{MC}; #DeltaR^2_{MC}", 100, 0, 0.004, 250, 0, 10000);
    
    TH2F *h_DRxy_spezzate = new TH2F("h_DRxy_spezzate","#DeltaR_{MC} x vs y tra i segmenti spezzati; #DeltaRx_{MC}; #DeltaRy_{MC}", 200, 0, 100, 200, 0, 100);
    TH2F *h_DTxy_spezzate = new TH2F("h_DTxy_spezzate","#DeltaT_{MC} tra i segmenti spezzati; #DeltaT_x_{MC};  #DeltaT_y_{MC}", 280, 0, 0.07, 280, 0, 0.07);
    
    TH1F *h_len_NO = new TH1F("h_len_NO","Lunghezza tracce con frac_MC<0.6; Npl; entries" , NPLATES+1, 0, NPLATES);

    
    cout << "Number of entries MC RECO: " << nentries_MCRECO << endl;
    // cout << "Number of entries MC: " << EventTree->GetEntries() << endl;
    int NO=0, lastW=0;
    float DTmax=0, DRmax=0, DTmax_temp=0, DRmax_temp=0;
    
    
    
    for (Long64_t itr=0;itr<nentries_MCRECO; itr++) { //nentries_MCRECO
        
        if(eVERBOSE==0 && (!(itr%5000))) cout << itr << " " << float(100*itr/nentries_MCRECO) << "\%" << endl;
        N_ok=0;
        N_ok_plusdau=0;
        
        NO=0;
        lastoksegplate=0;
        firstoksegplate=0;
        refplate=0;
        
        tmp_firstOK=0;
        tmp_lastOK=0;
        
        tree_MCRECO->GetEntry(itr);
        tr=tr1;
        nseg=nseg1;
        npl=npl1;
        n0=n01;
        w=w1;
        DTmax_temp=0;
        DRmax_temp=0;
        
        
        seg=(EdbSegP*)s->At(0);
        firstplate=seg->Vid(0) +1;
        lastplate=seg->Vid(1) +1;
        P_init=seg->P();
        th_init=seg->Theta();
        nseg_MC=lastplate-firstplate+1;
        mctrack=seg->MCTrack();
        mcevent=seg->MCEvt();
        pdgcode=seg->Aid(0);
        motherID=seg->Aid(1);

        
        if(eVERBOSE==1) cout << endl << "itr: " << itr << "\tPlate: " << tr->Plate() << "\tnseg: " << nseg << "\tnsegMC: " << nseg_MC << endl;
        if(eVERBOSE==1) cout << "\t" << "Evt" << "\t\t" << "MCTr" << "\t" << "pdg" << "\t" << "mumID" <<  "\t" << "P" << endl;
        for(Long64_t iseg=0;iseg<nseg; iseg++){
            seg=(EdbSegP*)s->At(iseg);
            if(iseg!=0) {
                seg1=(EdbSegP*)s->At(iseg-1);
                h_DT2_all_OK->Fill(DT_2(seg,seg1));
                h_DR2_all_OK->Fill(DR_2(seg,seg1));
                
                DTmax = std::max(DT_2(seg,seg1), DTmax_temp);
                DRmax = std::max(DR_2(seg,seg1), DRmax_temp);
                
                DTmax_temp = DTmax;
                DRmax_temp = DRmax;
            }
            
            if(eVERBOSE==1) cout << "\t" << mcevent << " " << seg->MCEvt() << "\t" << mctrack << " " << seg->MCTrack() << "\t" << pdgcode << " " << seg->Aid(0) << "\t" << motherID << " " << seg->Aid(1) << "\t" << seg->P();
            if(seg->MCTrack()==mctrack&&seg->MCEvt()==mcevent){
                if(eVERBOSE==1) cout << "\tok";
                if(iseg==0) firstoksegplate=seg->Plate();
                N_ok++;
                N_ok_plusdau++;
                lastW=0;
                lastoksegplate=seg->Plate();
                if(iseg==nseg-1) {lastOK++; tmp_lastOK=1;}
                if(iseg==0) {firstOK++; tmp_firstOK=1;}
            }
            else if(seg->Aid(1)==mctrack){
                N_ok_plusdau++;
                if(eVERBOSE==1) cout << "\tdau";
            }
            else{
                NO=1;
                lastW=1;
                if(eVERBOSE==1) cout << "\tNO";
                if(iseg==0) seg_prev=(EdbSegP*)s->At(iseg+1);
                else seg_prev=(EdbSegP*)s->At(iseg-1);
                
                h_P_DP_NO->Fill(seg->P(), seg_prev->P()-seg->P());
                h_th_Dth_NO->Fill(seg->Theta(), seg_prev->Theta()-seg->Theta());
            }
            
            if(eVERBOSE==1) cout << "\t" << seg->Plate() << "\t" << seg->TX() << " " << seg->TY() << "\t" << seg->X() << " " << seg->Y() << " " << seg->Z() << endl;
        }
        if(lastoksegplate-firstoksegplate+1>0.8*nseg_MC) recoOK++;
        
       // cout << itr << "\tmax DT: " << DTmax << endl;
       // cout << itr << "\tmax DR: " << DRmax << endl;

        
        h_DTmaxOK_P->Fill(DTmax, P_init);
        h_DRmaxOK_P->Fill(DRmax, P_init);
                
        frac=(float)N_ok/(float)nseg;
        frac_dau=(float)N_ok_plusdau/(float)nseg;
        if(motherID==-1)frac_oxy=(float)N_ok/(float)nseg;
        
        if(frac>0.95) N_1++;
        h_frac->Fill(frac);
        mean_frac+=frac;
        h_frac_oxy->Fill(frac_oxy);

        
        if(lastplate>=30) nseg_MC=NPLATES-tr->Plate()+1;
        
        fracMC=(float)N_ok/(float)nseg_MC;
        if(fracMC>0.95) N_1_MC++;
        else if(fracMC<0.6){
        h_th_NO->Fill(tr->Theta());
        h_P_NO->Fill(P_init);
        h_len_NO->Fill(npl);
        }
        if(motherID==-1)fracMC_oxy=(float)N_ok/(float)nseg;

        
        h_fracMC->Fill(fracMC);
        h_fracMC_oxy->Fill(fracMC_oxy);

        mean_fracMC+=fracMC;
        
        h_nseg_nsegMC->Fill((float)nseg/(float)nseg_MC);
        h_P_fracdau->Fill(P_init,frac_dau);
        h_th_fracdau->Fill(th_init, frac_dau);
        
        //cout << nseg << "\t" << nseg_MC << "\t" << nseg/nseg_MC << endl;
        if(eVERBOSE==1) cout << "\t\t--> ";
        if(eVERBOSE==1 && fracMC==1) cout << " OK ";
        if(eVERBOSE==1) cout << N_ok << "\t" << nseg << "\t" << frac << "\t" << nseg_MC << "\t" << fracMC << endl;
        
        //cout << "firstplate: " << firstplate << " firstoksegplate: " << firstoksegplate << "\tlastplate: " << lastplate << " lastoksegplate: " << lastoksegplate << endl;
        
        if( (fracMC!=1)&&(firstplate!=firstoksegplate) || (lastplate!=lastoksegplate && lastoksegplate!=30) ){
            ib1=0;
            ib2=0;
            EventTree->GetEntry(mcevent);
            if(mcevent==evt->GetID()){
                vtMC = evt->GetVolumeTrack(mctrack-1);
                
                if(eVERBOSE==1) cout << "\t\t\tMC: " << vtMC.GetID() << "\t" << vtMC.GetFlukaID() << "\t" << vtMC.GetBaseTracks().size() << "\t" << DegToRad(vtMC.GetAngle()) << " rad" << "\t" << firstoksegplate << " " << lastoksegplate << "\t" << vtMC.GetBaseTrack(0).GetLayer()+1 << " " << vtMC.GetBaseTrack(vtMC.GetBaseTracks().size()-1).GetLayer()+1;
                
                if(lastoksegplate==vtMC.GetBaseTrack(vtMC.GetBaseTracks().size()-1).GetLayer()+1||lastoksegplate==NPLATES){
                    
                    // if( (firstplate!=firstoksegplate) || (lastplate!=lastoksegplate) || (firstplate!=vtMC.GetBaseTrack(0).GetLayer()+1) || (lastplate!=vtMC.GetBaseTrack(vtMC.GetBaseTracks().size()-1).GetLayer()+1) ){
                    
                    refplate=firstoksegplate;
                }
                else {
                    refplate=lastoksegplate;
                }
                
                if(eVERBOSE==1) cout << "\tref pl: " << refplate << endl;
                
                ib1=0;
                ib2=0;
                ib3=0;
                
                for(int ib=0; ib<vtMC.GetBaseTracks().size(); ib++){
                    btMC=vtMC.GetBaseTrack(ib);
                    if (btMC.GetLayer()+1>=refplate-1&&btMC.GetLayer()+1<=refplate+1){
                        if(eVERBOSE==1) cout << "\t\t\t>";
                        if((btMC.GetLayer()+1==refplate-1)&&(vtMC.GetBaseTrack(0).GetLayer()+1<firstoksegplate)){
                            //cout << "\t\t\t>";
                            ib1=ib;
                        }
                        if((btMC.GetLayer()+1==refplate+1) && ( vtMC.GetBaseTrack(vtMC.GetBaseTracks().size()-1).GetLayer()+1>lastoksegplate)){
                            //cout << "\t\t\t>";
                            ib3=ib;
                        }
                        
                        
                        //                            if(btMC.GetLayer()+1==refplate-1 || btMC.GetLayer()+1==refplate+1){
                        //                                cout << "\t\t\t>";
                        //                                if(vtMC.GetBaseTrack(0).GetLayer()+1<firstoksegplate){
                        //                                ib1=ib;
                        //                                }
                        //                                else if(vtMC.GetBaseTrack(vtMC.GetBaseTracks().size()-1).GetLayer()+1>lastoksegplate){
                        //                                    ib1=ib;
                        //                                }
                        
                        if(btMC.GetLayer()+1==refplate) {
                            //cout << "\t\t\t>>>";
                            if(eVERBOSE==1) cout << ">>";
                            ib2=ib;
                        }
                        if(eVERBOSE==1) cout << " " << btMC.GetLayer()+1 << "\t" << btMC.GetX()* 1E+4 + 62500 << "\t" << btMC.GetY()* 1E+4 + 49500 << "\t" << btMC.GetSX() << "\t" << btMC.GetSY() << endl;
                    }
                }
                
                btMC=vtMC.GetBaseTrack(ib2);
                btMCpre=vtMC.GetBaseTrack(ib1);
                btMCnext=vtMC.GetBaseTrack(ib3);

                // if(eVERBOSE==1) cout << "\t\t\t\tDeltaR: " << DeltaR_proiezione(btMC.GetX(),btMCnext.GetX(),btMC.GetY(),btMCnext.GetY(),btMCnext.GetSX(),btMCnext.GetSY()) << "\tDeltaTh: " << DeltaPitagora(btMC.GetSX(),btMCnext.GetSX(),btMC.GetSY(),btMCnext.GetSY()) << endl;
                
                //cout << "seg MC: " << btMC.GetLayer()+1 << " " << btMC.GetZ() << "\t" << btMC.GetX() << " " << btMC.GetY() << endl;
                //cout << "seg MC next: " << btMCnext.GetLayer()+1 << " " << btMCnext.GetZ() << endl;
                //cout << "\t\t" << btMCnext.GetX() << " " << btMCnext.GetY() << "\t" << btMCnext.GetSX() << " " << btMCnext.GetSY() << endl;
                
                //cout << "check: " << btMC.GetLayer()+1 << " " << btMCnext.GetLayer()+1 << "\tDT: " << DT_OK(btMC, btMCnext) << endl;
                //cout << "DR: " << DR_OK(btMC, btMCnext) << endl;
                
                //cout << btMC.GetLayer()+1 << " " << btMCnext.GetLayer()+1 << "\tDT_2: " << DT_2(btMC, btMCnext) << "\tDR_2: " << DR_2(btMC, btMCnext) << "\tDT_x: " << DT_x(btMC, btMCnext) << "\tDT_y: " << DT_y(btMC, btMCnext) << "\tDR_x: " << DR_y(btMC, btMCnext) << "\tDR_y: " << DR_x(btMC, btMCnext) << endl;
                h_DT_spezzate->Fill(DT_2(btMC, btMCnext));
                h_DR_spezzate->Fill(DR_2(btMC, btMCnext));
                h_DthDR_spezzate->Fill(DT_2(btMC, btMCnext), DR_2(btMC, btMCnext));
                
                h_DTxy_spezzate->Fill(DT_x(btMC, btMCnext),DT_y(btMC, btMCnext));
                h_DRxy_spezzate->Fill(DR_x(btMC, btMCnext),DR_y(btMC, btMCnext));
                
                
                if(ib1!=0)DR_OK_res=DR_OK(btMCpre, btMC);
                if(ib1!=0)DT_OK_res=DT_OK(btMCpre, btMC);
                if(ib3!=0)DR_OK_res=DR_OK(btMC, btMCnext);
                if(ib3!=0)DT_OK_res=DT_OK(btMC, btMCnext);

                
                if(DR_OK_res==1) COUNT_DR_OK++;
                if(DT_OK_res==1) COUNT_DT_OK++;
                if(DR_OK_res==1&&DT_OK_res==1) COUNT_DRoDT_OK++;
                
                COUNT_tot_DRDT++;
                
                
            }
            else cout << "MC EvtID Not FOUND" << endl;
        }
        
        
        
        
    }
    mean_frac=mean_frac/(float)nentries_MCRECO;
    mean_fracMC=mean_fracMC/(float)nentries_MCRECO;
    
    cout << "First plate correct: " << (float)firstOK/nentries_MCRECO*100 << "\%" << endl;

    cout << "Last plate correct: " << (float)lastOK/nentries_MCRECO*100 << "\%" << endl;
    
    cout << "80% of the track reconstructed: " << (float)recoOK/nentries_MCRECO*100 << "\%" << endl;

    
    cout << "mean_frac: " << mean_frac << endl;
    cout << "mean_frac MC: " << mean_fracMC << endl;
    
    cout << "COUNT_DR_OK: " << COUNT_DR_OK << "\tCOUNT_DT_OK: " << COUNT_DT_OK << "\tCOUNT_DRoDT_OK: " << COUNT_DRoDT_OK << "\tToT: " << COUNT_tot_DRDT << endl;
    cout << "\% COUNT_DR_OK: " << (float)COUNT_DR_OK/COUNT_tot_DRDT << "\t\% COUNT_DT_OK: " << (float)COUNT_DT_OK/COUNT_tot_DRDT << "% COUNT_DRoDT_OK: " << (float)COUNT_DRoDT_OK/COUNT_tot_DRDT << endl;
    
    if(PLOT>=1){
        TAxis *axis = h_frac->GetXaxis();
        int bin_1 = axis->FindBin(1);
        double integral = h_frac->Integral(bin_1,bin_1);
        cout << endl << endl << "\% frac==1\t"<< (float)integral/(float)h_frac->GetEntries()*100 << endl;
        
        char title[100];
        TCanvas *c_frac = new TCanvas("c_frac", "c_frac", 900,900);
        c_frac->cd();
        c_frac->SetLogy();
        h_frac->Draw();
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_frac_trks.pdf");
        if(PLOT==1) c_frac->SaveAs(title);
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_frac_trks.C");
        if(PLOT==1) c_frac->SaveAs(title);
        
        axis = h_fracMC->GetXaxis();
        bin_1 = axis->FindBin(1);
        integral = h_fracMC->Integral(bin_1,bin_1);
        cout << endl << endl << "\% frac MC==1\t"<< (float)integral/(float)h_fracMC->GetEntries()*100 << endl;
        
        TCanvas *c_fracMC= new TCanvas("c_fracMC", "c_fracMC", 900,900);
        c_fracMC->cd();
        c_fracMC->SetLogy();
        h_fracMC->Draw();
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_fracMC_trks.pdf");
        if(PLOT==1) c_fracMC->SaveAs(title);
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_fracMC_trks.C");
        if(PLOT==1) c_fracMC->SaveAs(title);
        
        
        //
        axis = h_frac_oxy->GetXaxis();
        bin_1 = axis->FindBin(1);
        integral = h_frac_oxy->Integral(bin_1,bin_1);
        cout << endl << endl << "\% frac_oxy==1\t"<< (float)integral/(float)h_frac_oxy->GetEntries()*100 << endl;
        
        TCanvas *c_frac_oxy = new TCanvas("c_frac_oxy", "c_frac_oxy", 900,900);
        c_frac_oxy->cd();
        c_frac_oxy->SetLogy();
        h_frac_oxy->Draw();
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_frac_trks_oxy.pdf");
        if(PLOT==1) c_frac_oxy->SaveAs(title);
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_frac_trks_oxy.C");
        if(PLOT==1) c_frac_oxy->SaveAs(title);
        
        axis = h_fracMC_oxy->GetXaxis();
        bin_1 = axis->FindBin(1);
        integral = h_fracMC_oxy->Integral(bin_1,bin_1);
        cout << endl << endl << "\% frac MC_oxy==1\t"<< (float)integral/(float)h_fracMC_oxy->GetEntries()*100 << endl;
        
        TCanvas *c_fracMC_oxy= new TCanvas("c_fracMC_oxy", "c_fracM_oxyC", 900,900);
        c_fracMC_oxy->cd();
        c_fracMC_oxy->SetLogy();
        h_fracMC_oxy->Draw();
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_fracMC_trks_oxy.pdf");
        if(PLOT==1) c_fracMC->SaveAs(title);
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_fracMC_trks_oxy.C");
        if(PLOT==1) c_fracMC_oxy->SaveAs(title);
        //
        
        TCanvas *c_nseg_nsegMC= new TCanvas("c_nseg_nsegMC", "c_nseg_nsegMC", 900,900);
        c_nseg_nsegMC->cd();
        c_nseg_nsegMC->SetLogy();
        h_nseg_nsegMC->Draw();
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_nseg_nsegMC.pdf");
        if(PLOT==1) c_nseg_nsegMC->SaveAs(title);
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_nseg_nsegMC.C");
        if(PLOT==1) c_nseg_nsegMC->SaveAs(title);
        
        TCanvas *c_P_NO= new TCanvas("c_P_NO", "c_P_NO", 900,900);
        c_P_NO->cd();
        //c_P_NO->SetLogy();
        h_P_NO->Draw();
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_P_NO.pdf");
        if(PLOT==1) c_P_NO->SaveAs(title);
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_P_NO.C");
        if(PLOT==1) c_P_NO->SaveAs(title);
        
        TCanvas *c_P_DP_NO= new TCanvas("c_P_DP_NO", "c_P_DP_NO", 900,900);
        c_P_DP_NO->cd();
        h_P_DP_NO->Draw("COLZ");
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_P_DP_NO.pdf");
        if(PLOT==1) c_P_DP_NO->SaveAs(title);
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_P_DP_NO.C");
        if(PLOT==1) c_P_DP_NO->SaveAs(title);
        
        TCanvas *c_th_NO= new TCanvas("c_th_NO", "c_th_NO", 900,900);
        c_th_NO->cd();
        h_th_NO->Draw();
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_th_NO.pdf");
        if(PLOT==1) c_th_NO->SaveAs(title);
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_th_NO.C");
        if(PLOT==1) c_th_NO->SaveAs(title);
        
        TCanvas *c_th_Dth_NO= new TCanvas("c_th_Dth_NO", "c_th_Dth_NO", 900,900);
        c_th_Dth_NO->cd();
        h_th_Dth_NO->Draw("COLZ");
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_th_Dth_NO.pdf");
        if(PLOT==1) c_th_Dth_NO->SaveAs(title);
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_th_Dth_NO.C");
        if(PLOT==1) c_th_Dth_NO->SaveAs(title);
        
        TCanvas *c_P_fracdau = new TCanvas("c_P_fracdau", "c_P_fracdau", 900,900);
        c_P_fracdau->cd();
        h_P_fracdau->Draw("COLZ");
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_P_fracdau.pdf");
        if(PLOT==1) c_P_fracdau->SaveAs(title);
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_P_fracdau.C");
        if(PLOT==1) c_P_fracdau->SaveAs(title);
        
        TCanvas *c_th_fracdau = new TCanvas("c_th_fracdau", "c_th_fracdau", 900,900);
        c_th_fracdau->cd();
        h_th_fracdau->Draw("COLZ");
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_th_fracdau.pdf");
        if(PLOT==1) c_th_fracdau->SaveAs(title);
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_th_fracdau.C");
        if(PLOT==1) c_th_fracdau->SaveAs(title);
        
        TCanvas *c_DT2_all_OK= new TCanvas("c_DT2_all_OK", "c_DT2_all_OK", 900,900);
        c_DT2_all_OK->cd();
        h_DT2_all_OK->Draw();
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_DT2_all_OK.pdf");
        if(PLOT==1) c_DT2_all_OK->SaveAs(title);
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_DT2_all_OK.C");
        if(PLOT==1) c_DT2_all_OK->SaveAs(title);
        
        TCanvas *c_DR2_all_OK= new TCanvas("c_DR2_all_OK", "c_DR2_all_OK", 900,900);
        c_DR2_all_OK->cd();
        h_DR2_all_OK->Draw();
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_DR2_all_OK.pdf");
        if(PLOT==1) c_DR2_all_OK->SaveAs(title);
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_DR2_all_OK.C");
        if(PLOT==1) c_DR2_all_OK->SaveAs(title);
        
        TCanvas *c_DTmaxOK_P= new TCanvas("c_DTmaxOK_P", "c_DTmaxOK_P", 900,900);
        c_DTmaxOK_P->cd();
        h_DTmaxOK_P->Draw("COLZ");
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_DTmaxOK_P.pdf");
        if(PLOT==1) c_DTmaxOK_P->SaveAs(title);
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_DTmaxOK_P.C");
        if(PLOT==1) c_DTmaxOK_P->SaveAs(title);
        
        TCanvas *c_DRmaxOK_P= new TCanvas("c_DRmaxOK_P", "c_DRmaxOK_P", 900,900);
        c_DRmaxOK_P->cd();
        h_DRmaxOK_P->Draw("COLZ");
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_DRmaxOK_P.pdf");
        if(PLOT==1) c_DRmaxOK_P->SaveAs(title);
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_DRmaxOK_P.C");
        if(PLOT==1) c_DRmaxOK_P->SaveAs(title);
        
        TCanvas *c_DT_spezzate= new TCanvas("c_DT_spezzate", "c_DT_spezzate", 900,900);
        c_DT_spezzate->cd();
        h_DT_spezzate->Draw();
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_DT_spezzate.pdf");
        if(PLOT==1) c_DT_spezzate->SaveAs(title);
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_DT_spezzate.C");
        if(PLOT==1) c_DT_spezzate->SaveAs(title);
        
        TCanvas *c_DR_spezzate= new TCanvas("c_DR_spezzate", "c_DR_spezzate", 900,900);
        c_DR_spezzate->cd();
        h_DR_spezzate->Draw();
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_DR_spezzate.pdf");
        if(PLOT==1) c_DR_spezzate->SaveAs(title);
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_DR_spezzate.C");
        if(PLOT==1) c_DR_spezzate->SaveAs(title);
        
        TCanvas *c_DRxy_spezzate= new TCanvas("c_DRxy_spezzate", "c_DRxy_spezzate", 900,900);
        c_DRxy_spezzate->cd();
        //h_DRxy_spezzate->SetMarkerSize(7);
        h_DRxy_spezzate->Draw("COLZ");
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_DRxy_spezzate.pdf");
        if(PLOT==1) c_DRxy_spezzate->SaveAs(title);
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_DRxy_spezzate.C");
        if(PLOT==1) c_DRxy_spezzate->SaveAs(title);
        
        TCanvas *c_DTxy_spezzate= new TCanvas("c_DTxy_spezzate", "c_DTxy_spezzate", 900,900);
        c_DTxy_spezzate->cd();
        //h_DTxy_spezzate->SetMarkerSize(7);
        h_DTxy_spezzate->Draw("COLZ");
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_DTxy_spezzate.pdf");
        if(PLOT==1) c_DTxy_spezzate->SaveAs(title);
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_DTxy_spezzate.C");
        if(PLOT==1) c_DTxy_spezzate->SaveAs(title);
        
        
        
        //    TCanvas *c_nseg = new TCanvas("c_nseg", "c_nseg", 1500,900);
        //    TCanvas *c_Energy = new TCanvas("c_Energy", "c_Energy", 1500,900);
        //    TCanvas *c_Angle = new TCanvas("c_Angle", "c_Angle", 1500,900);
        //    TCanvas *c_impactfac = new TCanvas("c_impactfac", "c_impactfac", 1500,900);
        
        TCanvas *c_DthDR_spezzate= new TCanvas("c_DthDR_spezzate", "c_DthDR_spezzate", 900,900);
        c_DthDR_spezzate->cd();
        h_DthDR_spezzate->Draw("COLZ");
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_DthDR_spezzate.pdf");
        if(PLOT==1) c_DthDR_spezzate->SaveAs(title);
        sprintf(title, "/home/scanner/foot/RECO_MC_TEST_giul/b000002/plot_fedraMC/h_DthDR_spezzate.C");
        if(PLOT==1) c_DthDR_spezzate->SaveAs(title);
    }
}



//float proietta(float X, float Y, float Z){
//    Float_t dz=190;
//    //cout << "dz " << dz << endl;
//    X2=X1-dz*TX1;
//    Y2=Y1-dz*TY1;
//}



int DT_OK(EMU_BaseTrack btMC, EMU_BaseTrack btMCnext){
    float sx = btMC.GetPX()/btMC.GetPZ();
    float sx_next = btMCnext.GetPX()/btMCnext.GetPZ();
    float sy = btMC.GetPY()/btMC.GetPZ();
    float sy_next = btMCnext.GetPY()/btMCnext.GetPZ();
    
    float dtx = sx - sx_next;
    float dty = sy - sy_next;
    double dt2 = dtx*dtx +  dty*dty;
    
    // cout << "btMC.GetLayer(): " << btMC.GetLayer() << "\tbtMCnext.GetLayer(): " << btMCnext.GetLayer() << endl;
    // cout << "btMC.GetSX(): " << btMC.GetSX() << " btMC.GetSY(): " << btMC.GetSY() << "\tbtMCnext.GetSX(): " << btMCnext.GetSX() << " btMCnext.GetSY(): " << btMCnext.GetSY() << endl;
    
    if(eVERBOSE==1) cout << "DT: " << dtx << " " << dty << " " << sqrt(dt2) << endl;
    
    if( TMath::Abs( dtx ) > eDTmax )    return 0;
    if( TMath::Abs( dty ) > eDTmax )    return 0;
    if(dt2>eDTmax*eDTmax)        return 0;
    
    return 1;
}


float DT_x(EMU_BaseTrack btMC, EMU_BaseTrack btMCnext){
    float sx = btMC.GetPX()/btMC.GetPZ();
    float sx_next = btMCnext.GetPX()/btMCnext.GetPZ();
    return TMath::Abs(sx - sx_next);
}


float DT_y(EMU_BaseTrack btMC, EMU_BaseTrack btMCnext){
    float sy = btMC.GetPY()/btMC.GetPZ();
    float sy_next = btMCnext.GetPY()/btMCnext.GetPZ();
    return TMath::Abs(sy - sy_next);
}

float DT_2(EMU_BaseTrack btMC, EMU_BaseTrack btMCnext){
    float sx = btMC.GetPX()/btMC.GetPZ();
    float sx_next = btMCnext.GetPX()/btMCnext.GetPZ();
    float sy = btMC.GetPY()/btMC.GetPZ();
    float sy_next = btMCnext.GetPY()/btMCnext.GetPZ();
    
    float dtx = sx - sx_next;
    float dty = sy - sy_next;
    // cout << "res. " << dtx << "\t" << dty << "\t" << dtx*dtx + dty*dty << endl;
    return sqrt(dtx*dtx + dty*dty);
}

float DT_2(EdbSegP *btMC, EdbSegP *btMCnext){
    
    float sx = btMC->TX();
    float sx_next = btMCnext->TX();
    float sy = btMC->TY();
    float sy_next = btMCnext->TY();
    
    float dtx = sx - sx_next;
    float dty = sy - sy_next;
    return sqrt(dtx*dtx + dty*dty);
    
}

int DR_OK(EMU_BaseTrack btMC, EMU_BaseTrack btMCnext){
    
    float sx = btMC.GetPX()/btMC.GetPZ();
    float sy = btMC.GetPY()/btMC.GetPZ();
    float dz = btMCnext.GetZ()-btMC.GetZ();
    
    float dx = btMCnext.GetX()* 1E+4 + 62500 - (btMC.GetX()* 1E+4 + 62500 + dz*sx);
    float dy = btMCnext.GetY()* 1E+4 + 49500 - (btMC.GetY()* 1E+4 + 49500 + dz*sy);
    double dr2 = dx*dx + dy*dy;
    
    // cout << "btMC.GetX(): " << btMC.GetX()* 1E+4 + 62500 << " (btMC.GetY()* 1E+4 + 49500): " << btMC.GetY()* 1E+4 + 49500 << "\tbtMCnext.GetX(): " << btMCnext.GetX()* 1E+4 + 62500 << " (btMCnext.GetY()* 1E+4 + 49500): " << btMCnext.GetY()* 1E+4 + 49500 << endl;
    
    if(eVERBOSE==1) cout << btMC.GetLayer()+1 << "-" << btMCnext.GetLayer()+1 << endl;
    if(eVERBOSE==1) cout << "DR: " << dx << " " << dy << " " << sqrt(dr2) << endl;
    
    if( TMath::Abs( dx ) > eDRmax ) return 0;
    if( TMath::Abs( dy ) > eDRmax ) return 0;
    if(dr2>eDRmax*eDRmax) return 0;
    
    return 1;
}

float DR_x(EMU_BaseTrack btMC, EMU_BaseTrack btMCnext){
    float sx = btMC.GetPX()/btMC.GetPZ();
    float dz = btMCnext.GetZ()-btMC.GetZ();
    return TMath::Abs((btMCnext.GetX()* 1E+4 + 62500) - ((btMC.GetX()* 1E+4 + 62500) + dz*sx));
}

float DR_y(EMU_BaseTrack btMC, EMU_BaseTrack btMCnext){
    float sy = btMC.GetPY()/btMC.GetPZ();
    float dz = btMCnext.GetZ()-btMC.GetZ();
    return TMath::Abs((btMCnext.GetY()* 1E+4 + 49500) - ((btMC.GetY()* 1E+4 + 49500) + dz*sy));
}

float DR_2(EMU_BaseTrack btMC, EMU_BaseTrack btMCnext){
    float sx = btMC.GetPX()/btMC.GetPZ();
    float sy = btMC.GetPY()/btMC.GetPZ();
    float dz = btMCnext.GetZ()-btMC.GetZ();
    
    float dx = (btMCnext.GetX()* 1E+4 + 62500) - ((btMC.GetX()* 1E+4 + 62500) + dz*sx);
    float dy = (btMCnext.GetY()* 1E+4 + 49500) - ((btMC.GetY()* 1E+4 + 49500) + dz*sy);
    return sqrt(dx*dx + dy*dy);
}

float DR_2(EdbSegP *btMC, EdbSegP *btMCnext){
    float sx = btMC->TX();
    float sy = btMC->TY();
    float dz = btMCnext->Z() - btMC->Z();
    
    float dx = btMCnext->X() - btMC->X()+ dz*sx;
    float dy = btMCnext->Y() - btMC->Y() + dz*sy;
    return sqrt(dx*dx + dy*dy);
}




