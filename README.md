# List of scripts to analyze emulsion data from FOOT 2019 GSI testbeam

Using FEDRA set of ROOT libraries. Standard ScanSet file format

Scripts are currently being written by Antonio, Brenda and Giuliana.

## Data processing

Please refer to `fedra_workflow.md` for details, steps are basically:

* linking (two steps);

* alignment (two steps);

* tracking;

* vertexing