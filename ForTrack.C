void ForTrack() {

  EdbSegP *tr =NULL;
  int nseg;
  
  TFile * trk = TFile::Open("/home/scanner/foot/2019_GSI/GSI2/b000002/b000002.0.0.0.trk.root");
  TTree * tree = (TTree*) trk->Get("tracks");
  int nentries = tree->GetEntries();
  
  TH2F * hxy = new TH2F("hxy","XY positions of the track",200,20000,100000, 200,10000,90000);
  
  tree->SetBranchAddress("t.",&tr);
  tree->SetBranchAddress("nseg",&nseg);
  
  for( int i=0 ; i<nentries; i++){
    tree->GetEntry(i);
    
    hxy->Fill(tr->X(),tr->Y());  
    
     cout<<i <<" "<< tr->X()<<" "<<tr->Y() << endl;
  }    
  //  hxy->GetXaxis()->SetTitle("x[mm]");
  //hxy->GetYaxis()->SetTitle("y[mm]");
  
  hxy->Draw("COLZ");
 
}
