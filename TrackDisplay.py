#using fedra methods to build the EdbTrackP list from the file
import ROOT
import fedrarootlogon
import sys
#usage: python -i VerteTrackDisplay.py -f vertexfile -t tracksfile -nt trackIDS -nv vertexIDS

from argparse import ArgumentParser #not present in good old nusrv9, but the commands should work in a reasonable python setup, only need to remove the parser and options comments,then comment the sys.argv lines

dproc = ROOT.EdbDataProc()
gAli = dproc.PVR()

fedratrackslist = []
#vertexnumberlist = [10, 20]
isolatedtrackcolors = [ROOT.kMagenta, ROOT.kMagenta, ROOT.kMagenta, ROOT.kMagenta, ROOT.kMagenta, ROOT.kMagenta, ROOT.kMagenta] #so we can set different colors for different tracks
parser = ArgumentParser()
parser.add_argument("-t", "--tracks", dest="tracksfilename", help="file with fedra tracks",
                    required=True)
parser.add_argument("-nt", "--ntracks", nargs='*', dest="tracklist", help="number of isolated tracks to display")
parser.add_argument("-new", action='store_true') #for new file format

options = parser.parse_args()
if (options.tracklist): 
 fedratrackslist = options.tracklist
 fedratrackslist = map(int, fedratrackslist)
tracksfilename = options.tracksfilename


inputfile = ROOT.TFile.Open(tracksfilename,"READ")
tracktree = inputfile.Get("tracks")
tracktree.SetAlias("trk","t.") #points create confusion to python

tracktree.BuildIndex("trid")
tracks = ROOT.TObjArray(100)
for trackID in fedratrackslist:
 tracktree.GetEntryWithIndex(trackID)
 #temporary object for reading the file and building EdbTrackP
 temptrack = ROOT.EdbTrackP()
 temptrack .Copy(ROOT.EdbTrackP(tracktree.trk))
 segments = tracktree.s
 fittedsegments = tracktree.sf
 #loop on segments associated to the track
 for seg,segf in zip(segments, fittedsegments):
     temptrack .AddSegment(seg)
     seg.SetDZ(300)
     segf.SetDZ(300)
     print("Test DZ", segf.DZ())
     temptrack .AddSegmentF(segf)
     temptrack .SetSegmentsTrack(temptrack.ID())
     temptrack .SetCounters()
 tracks.Add(temptrack)

def drawtracks(tracks):
 #ds.SetVerRec(gEVR);
 ds.SetDrawTracks(4)
 ds.SetArrTr(tracks )
 ds.Draw()

 
             

ROOT.gStyle.SetPalette(1);
dsname="Charm simulation FEDRA display"
ds = ROOT.EdbDisplay.EdbDisplayExist(dsname);
if not ds:  
  ds=ROOT.EdbDisplay(dsname,-50000.,50000.,-50000.,50000.,-4000.,80000.)
drawtracks(tracks)



