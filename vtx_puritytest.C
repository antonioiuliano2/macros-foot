/*
Questo script vuole valutare la purezza dei vertici (MC) ricostruiti. Il nome del file di vertici da usare è all’interno del codice. Produce plot di molteplicità, parametro di impatto di beam / daughters per tracce giuste e sbagliate, plot con la frazione di tracce giuste per vertice
 
 Usage: root -l vtx_puritytest.C
 
 scp /Users/Giuliana/Desktop/Uni/FOOT/GSI_data/vtx_puritytest.C scanner@nusrv9.na.infn.it:/home/scanner/foot/RECO_MC_TEST_giul/b000002
 
 scp /Users/Giuliana/Desktop/Uni/FOOT/GSI_data/vtx_puritytest.C scanner@nusrv9.na.infn.it:/home/scanner/foot/2019_GSI/macros
 
 pw: neutrino.01
 
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <map>
#include <string>
#include <utility>
#include <string>
#include "TH1.h"
#include "TCanvas.h"
#include "TVector.h"
#include "TVector3.h"
#include "TLegend.h"
#include "TFile.h"
#include "TString.h"
#include "TObjString.h"
#include "TSystem.h"
#include "TROOT.h"
//FEDRA libraries
#include "/mnt/sdb/opera/ana/valeri/sw_oracle_dev/fedra/include/EdbSegP.h"
#include "/mnt/sdb/opera/ana/valeri/sw_oracle_dev/fedra/include/EdbPattern.h"

void vtx_puritytest(){
    
    //leggo il file di vertici in input
    TString inputfile_vtxname = "vertices_improved_fast_3.root";
    
    TFile *inputfile_vtx = TFile::Open(inputfile_vtxname,"READ");
    if (inputfile_vtx == NULL) cout<<"ERROR: inputfile_vtx not found"<<endl;
    EdbVertexRec *vrec = (EdbVertexRec*) inputfile_vtx->Get("EdbVertexRec");
    EdbPVRec *ali = new EdbPVRec();
    EdbScanCond *myscancond = new EdbScanCond();
    ali->SetScanCond(myscancond);
    EdbDataProc *dproc = new EdbDataProc();
    vrec->SetPVRec(ali);
    
    const int nvertices = vrec->eVTX->GetEntries();
    
    
    cout << "Using vertices file: " << inputfile_vtxname << endl;
    cout << "Analysing " << nvertices << " vertices" << endl;
    
    TH1F *h_frac = new TH1F("h_frac","frac; frac; events",9,0.2,1.1);
    TH1I *h_ntrks = new TH1I("h_ntrks","ntrks; ntrks; events",20,0,20);
    
    TH1F *h_IPtrue_Beam = new TH1F("h_IPtrue_Beam","Impact Parameter Beam; Impact Parameter; events",150/5,0,150);
    TH1F *h_IPfalse_Beam = new TH1F("h_IPfalse_Beam","Impact Parameter; Impact Parameter; events",150/5,0,150);
    TH1F *h_IPtrue_Dau = new TH1F("h_IPtrue_Dau","Impact Parameter Beam; Impact Parameter; events",150/5,0,150);
    TH1F *h_IPfalse_Dau = new TH1F("h_IPfalse_Dau","Impact Parameter; Impact Parameter; events",150/5,0,150);
    
    int ntracks_event[13000] = {0};
    int mostfrequentevent[13000] = {0};
    
    for (int ivtx = 0; ivtx < nvertices; ivtx++){
        
        map<int,int> frequencyEvent;
        map<int,int>::iterator it;
        ntracks_event[ivtx] = 0;
        mostfrequentevent[ivtx] = -1;
        
        EdbVertex *vertex = (EdbVertex *)(vrec->eVTX->At(ivtx));
        
        
        h_ntrks->Fill(vertex->N());
        
        if(vertex->N()>2){
            
            cout << endl << ivtx << "\t" << vertex->ID() << "\t" << vertex->N() << endl;

            
            for (int itrk = 0; itrk < vertex->N(); itrk++){
                EdbTrackP *track = vertex->GetTrack(itrk);
                int eventtrack=-99;
                if(vertex->GetVTa(itrk)->Zpos()==1){ //se è Beam prendo il primo segmento
                    eventtrack = track->GetSegmentFirst()->MCEvt();
                }
                else eventtrack = track->GetSegmentLast()->MCEvt(); //se è beam prendo l'ultimo segmento

                cout << "\t" << itrk << "\t" << eventtrack << "\t" << vertex->GetVTa(itrk)->Zpos() << "\t" << vertex->GetVTa(itrk)->Imp() << "\t" << track->GetSegment(0)->Plate() << "\t" << track->N() << "\t" << track->GetSegment(0)->W()-70 << "\t" << track->Theta() << endl;
                frequencyEvent[eventtrack]++;
            }
            
            cout << endl;
            for (it = frequencyEvent.begin(); it!=frequencyEvent.end();it++){
                cout << "\t\t" << it->first << "\t" << it->second << endl;
                if(it->second > ntracks_event[ivtx]){
                    ntracks_event[ivtx] = it->second;
                    mostfrequentevent[ivtx] = it->first;
                }
            }
            float frac = (float)ntracks_event[ivtx]/(float)vertex->N();
            cout << "\t" << frac << endl;
            
            h_frac->Fill(frac);
            
            
            for (int itrk = 0; itrk < vertex->N(); itrk++){
                EdbTrackP *track = vertex->GetTrack(itrk);
                int eventtrack=-99;
                if(vertex->GetVTa(itrk)->Zpos()==1){ //se è Dau prendo il primo segmento
                    eventtrack = track->GetSegmentFirst()->MCEvt();
                    if(mostfrequentevent[ivtx]==eventtrack){
                        h_IPtrue_Dau->Fill(vertex->GetVTa(itrk)->Imp());
                    }
                    else h_IPfalse_Dau->Fill(vertex->GetVTa(itrk)->Imp());

                }
                else {
                    eventtrack = track->GetSegmentLast()->MCEvt(); //se è beam prendo l'ultimo segmento
                    if(mostfrequentevent[ivtx]==eventtrack){
                    h_IPtrue_Beam->Fill(vertex->GetVTa(itrk)->Imp());
                    }
                    else h_IPfalse_Beam->Fill(vertex->GetVTa(itrk)->Imp());

                }
                
                
                cout << "\t" << itrk << "\t" << eventtrack << "\t" << vertex->GetVTa(itrk)->Zpos() << "\t" << vertex->GetVTa(itrk)->Imp() << "\t" << track->GetSegment(0)->Plate() << "\t" << track->N() << "\t" << track->GetSegment(0)->W()-70 << "\t" << track->Theta() << endl;
                frequencyEvent[eventtrack]++;
            }
            
            

            
        }
    }
    
    TBox *box;
    char txt[300];
    char plot_title[300];

    TCanvas *c_IP_Dau = new TCanvas("c_IP_Dau", "c_IP_Dau", 600,500);
    c_IP_Dau->cd();
    c_IP_Dau->SetTitle("IP Beam");
    h_IPtrue_Dau->SetTitle("IP Beam");
    h_IPtrue_Dau->SetLineWidth(2);
    h_IPtrue_Dau->SetLineColor(kRed);
    h_IPfalse_Dau->SetLineWidth(2);
    h_IPfalse_Dau->SetLineColor(kBlue);
    h_IPtrue_Dau->Sumw2();
    h_IPfalse_Dau->Sumw2();
    h_IPtrue_Dau->Draw("histo");
    h_IPfalse_Dau->Draw("sames");
    TLegend *legend_IP_Dau = new TLegend(.52,.50,.9,.9);
    legend_IP_Dau->AddEntry(h_IPtrue_Dau,"True");
    sprintf(txt,"#Entries: %.0f",h_IPtrue_Dau->GetEntries());
    legend_IP_Dau->AddEntry((TObject*)0, txt, "");
    sprintf(txt,"Mean: %.3f",h_IPtrue_Dau->GetMean());
    legend_IP_Dau->AddEntry((TObject*)0, txt, "");
    sprintf(txt,"RMS: %.3f",h_IPtrue_Dau->GetRMS());
    legend_IP_Dau->AddEntry((TObject*)0, txt, "");
    legend_IP_Dau->AddEntry(h_IPfalse_Dau,"False");
    sprintf(txt,"#Entries: %.0f",h_IPfalse_Dau->GetEntries());
    legend_IP_Dau->AddEntry((TObject*)0, txt, "");
    sprintf(txt,"Mean: %.3f",h_IPfalse_Dau->GetMean());
    legend_IP_Dau->AddEntry((TObject*)0, txt, "");
    sprintf(txt,"RMS: %.3f",h_IPfalse_Dau->GetRMS());
    legend_IP_Dau->AddEntry((TObject*)0, txt, "");
    legend_IP_Dau->SetTextSize(0.025);
    legend_IP_Dau->Draw("histosames");
    sprintf(plot_title,"vtx_plot/c_IP_Dau.pdf");
    c_IP_Dau->SaveAs(plot_title);
    sprintf(plot_title,"vtx_plot/c_IP_Dau.C");
    c_IP_Dau->SaveAs(plot_title);
    
    
    
    TCanvas *c_IP_Beam = new TCanvas("c_IP_Beam", "c_IP_Beam", 600,500);
    c_IP_Beam->cd();
    c_IP_Beam->SetTitle("IP Beam");
    h_IPtrue_Beam->SetTitle("IP Beam");
    h_IPtrue_Beam->SetLineWidth(2);
    h_IPtrue_Beam->SetLineColor(kRed);
    h_IPfalse_Beam->SetLineWidth(2);
    h_IPfalse_Beam->SetLineColor(kBlue);
    h_IPtrue_Beam->Sumw2();
    h_IPfalse_Beam->Sumw2();
    h_IPtrue_Beam->Draw("histo");
    h_IPfalse_Beam->Draw("sames");
    TLegend *legend_IP_Beam = new TLegend(.52,.50,.9,.9);
    legend_IP_Beam->AddEntry(h_IPtrue_Beam,"True");
    sprintf(txt,"#Entries: %.0f",h_IPtrue_Beam->GetEntries());
    legend_IP_Beam->AddEntry((TObject*)0, txt, "");
    sprintf(txt,"Mean: %.3f",h_IPtrue_Beam->GetMean());
    legend_IP_Beam->AddEntry((TObject*)0, txt, "");
    sprintf(txt,"RMS: %.3f",h_IPtrue_Beam->GetRMS());
    legend_IP_Beam->AddEntry((TObject*)0, txt, "");
    legend_IP_Beam->AddEntry(h_IPfalse_Beam,"False");
    sprintf(txt,"#Entries: %.0f",h_IPfalse_Beam->GetEntries());
    legend_IP_Beam->AddEntry((TObject*)0, txt, "");
    sprintf(txt,"Mean: %.3f",h_IPfalse_Beam->GetMean());
    legend_IP_Beam->AddEntry((TObject*)0, txt, "");
    sprintf(txt,"RMS: %.3f",h_IPfalse_Beam->GetRMS());
    legend_IP_Beam->AddEntry((TObject*)0, txt, "");
    legend_IP_Beam->SetTextSize(0.025);
    legend_IP_Beam->Draw("histosames");
 
    sprintf(plot_title,"vtx_plot/c_IP_Beam.pdf");
    c_IP_Beam->SaveAs(plot_title);
    sprintf(plot_title,"vtx_plot/c_IP_Beam.C");
    c_IP_Beam->SaveAs(plot_title);
    
    
    TCanvas *c_frac = new TCanvas("c_frac", "c_frac", 600,500);
    c_frac->cd();
    h_frac->Draw("");
    c_frac->SaveAs("h_frac.C");
    c_frac->SaveAs("h_frac.pdf");
    
    TCanvas *c_ntrks = new TCanvas("c_ntrks", "c_ntrks", 600,500);
    c_ntrks->cd();
    h_ntrks->Draw("");
    c_ntrks->SaveAs("h_ntrks.C");
    c_ntrks->SaveAs("h_ntrks.pdf");
    
    
}
